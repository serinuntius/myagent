import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import com.onformative.yahooweather.*;

PImage haikei;
////////////////////////////////////////////////////////
//時間割
////////////////////////////////////////////////////////
String mon[],tue[],wed[],thu[],fri[];
int year=year();
int day=day();
int m=month();
int cod;

////////////////////////////////////////////
//capture
////////////////////////////////////////////
Capture video;
OpenCV opencv;
int morning=0,afternoon=0,evening=0;
PImage imgr,imgl,imgu;
PImage angry,smile,cry,jito,odoroki,ase,gimon;
boolean Rflag=false,Lflag=false,Uflag=false;
int hour = hour();
Rectangle[] faces;
//int min = minute();
////////////////////////////////////////
//天気
////////////////////////////////////////
YahooWeather weather;
int updateIntervallMillis = 30000; 
PImage ame,kumori,yuki,kumo,kaminari,hare,taihu,mizore,kiri,wind,samui,atsui;



void setup() {
  size(1280, 720);
  video = new Capture(this, 640/2, 480/2);
  
  opencv = new OpenCV(this, 640/2, 480/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
  frameRate(30);  
  morning =int(random(1,10));
  afternoon =int(random(1,10));
  evening =int(random(1,10));
  video.start();
  ///////////////  ///////////////
  //エージェント画像読み込み
  ///////////////  ///////////////
  imgr = loadImage("FukiMigi.png");
  imgl = loadImage("FukiHidari2.png");
  imgu = loadImage("FukiShita.png");
  angry = loadImage("angry.png");
  smile = loadImage("smile.png");
  cry = loadImage("cry.png");
  jito = loadImage("jito.png");
  odoroki = loadImage("odoroki.png");
  ase = loadImage("ase.png"); 
  gimon = loadImage("gimon.png"); 

  //////////////////////////
  //時間割    
  //////////////////////////
  mon = loadStrings("1.txt");
  tue = loadStrings("2.txt");
  wed = loadStrings("3.txt");
  thu = loadStrings("4.txt");
  fri = loadStrings("5.txt");
  if(m <=2){
    m += 12;
    --year;
  }
  cod= (year + int(year/4) - int(year/100) + int(year/400) + int((13*m+8)/5) + day) % 7;
  //////////////////////////////
  //天気
  //////////////////////////////
  weather = new YahooWeather(this, 26282454, "c", updateIntervallMillis);
  hare = loadImage("hare.png");
  ame = loadImage("ame.png");
  kumori = loadImage("kumori.png");
  kaminari = loadImage("kaminari.png");
  yuki = loadImage("yuki.png");
  taihu = loadImage("taihu.png");
  mizore = loadImage("mizore.png");
  kiri = loadImage("kiri.png");
  wind = loadImage("wind.png");
  samui = loadImage("samui.png");
  atsui = loadImage("yuki.png");
  
  haikei = loadImage("haikei.jpg");
  
}

void draw() {

  scale(2);
  opencv.loadImage(video);
  background(255);
  println(width/2+160);
  //image(haikei,0,0);
  scale(-1, 1);
  image(video, -500, 0);
  scale(-1,1);

  noFill();
  stroke(0, 255, 0);
  strokeWeight(3);
  faces = opencv.detect();
  println(faces.length);
  println("frame"+frameRate);


  zikanwari();
  weather();
  tokei();


  if(faces.length==1){
    for (int i = 0; i < faces.length; i++) {
      println("width"+faces[i].width);
      println("x"+faces[i].x);
      rect(faces[i].x-500, faces[i].y, faces[i].width, faces[i].height);
      
      if(faces[i].x+faces[i].width/2 >215){
        Rflag=true;
        Lflag=false;
        Uflag=false;
        println("R");
      }
      
      else if(faces[i].x+faces[i].width/2 < 229 && faces[i].x+faces[i].width/2 > 130){
        Uflag=true;
        Lflag=false;
        Rflag=false;
        println("U");
    }
      
        else{
          Lflag=true;
          Rflag=false;
          Uflag=false;
          println("L");
        }
       
        println("length"+(faces[i].x+faces[i].width/2)); 
      if(Rflag){      
        image(imgr,faces[i].x+150,faces[i].y+2);
        fill(0);
        //text("進捗どうですか？？",faces[i].x+180,faces[i].y+20,80,70);
        println("Rimg");
        fukidashi(faces[i].x+180,faces[i].y+30,Time());
      }
      if(Uflag){
        image(imgu,faces[i].x+130,faces[i].y+120);
        fill(3);
        //text("おハガキはこちらまで！",faces[i].x+150,faces[i].y+170);
        fukidashi(faces[i].x+150,faces[i].y+155,Time());
        
        println("Uimg");
      }
      if(Lflag){   
        image(imgl,faces[i].x+120,faces[i].y);
        fill(0);
        // text("気合を入れろ",faces[i].x+150,faces[i].y+28);
        println("Limg");
        fukidashi(faces[i].x+150,faces[i].y+28,Time());

     }

    }
  }
}


void captureEvent(Capture c) {
  c.read();
}

int Time(){
  if(hour>6 && hour<12){
    return 1; 
  }
  else if(hour>12 && hour<19){
    return 2;
  }
  else{
    return 3;
  }
}

void fukidashi(int x,int y,int timezone){
  //timezone==1は朝
  if(frameCount % 50 == 0){
     morning =int(random(1,11));
     afternoon =int(random(1,11));
     evening =int(random(1,11));
  }
  if(timezone ==1){
    if(morning == 1){
      text("傘持ったか？？",x,y);
      emotion(gimon, x, y);
    }if(morning==2){
      text("１限間に合うか\nコレ！？",x,y);
      emotion(ase, x, y);
    }if(morning==3){
      text("小テストあるんだよ\nなぁ",x,y);
      emotion(cry, x, y);
    }
    if(morning==4){
      text("電車が\nヤバいゾ・・・",x,y);
      emotion(jito, x, y);
    }
    if(morning==5){
      text("貸すもの返すもの\n忘るるべからず",x,y);
      emotion(angry, x, y);
    }
    if(morning==6){
      text("鼻毛出てるで＾＾",x,y);
      emotion(jito, x, y);
    }
    if(morning==7){
      text("昼休み\n予定あるんじゃない？",x,y);
      emotion(gimon, x, y);
    }
    if(morning==8){
      text("語学の時は\nバス混むぞ！",x,y);
      emotion(odoroki, x, y);
    }
    if(morning==9){
      text("寝落ち\nしてんじゃねーよ！",x,y);
      emotion(angry, x, y);
    }
    if(morning==10){
      text("お兄ちゃん起きて～",x,y);
      emotion(smile, x, y);
    }
  }
  
  else if(timezone == 2){
    if(afternoon==1){
      text("昼飯\n何食べる？",x,y); 
      emotion(gimon, x, y); 
    }
    if(afternoon==2){
      text("遅すぎた起床",x,y);
      emotion(jito, x, y);  
    }
    if(afternoon==3){
      text("アーメン\nテストは終わったよ",x,y);
      emotion(odoroki, x, y);  
    }
    if(afternoon==4){
      text("アイマス最高！！",x,y);
      emotion(smile, x, y);  
    }
    if(afternoon==5){
      text("プレゼミ\n行かなきゃ",x,y);
      emotion(odoroki, x, y);  
    }
    if(afternoon==6){
      text("提出物は\n今日の１７時までや",x,y);
      emotion(cry, x, y);  
    }
    if(afternoon==7){
      text("午前中は\n何してたんだ＾＾",x,y);
      emotion(smile, x, y);  
    }
    if(afternoon==8){
      text("自主休校か？？",x,y);
      emotion(gimon, x, y);  
    }
    if(afternoon==9){
      text("バイトの用意\nそろそろすんぞ",x,y);
      emotion(jito, x, y);  
    }
    if(afternoon==10){
      text("パソコンは\n遊び道具か？？",x,y); 
      emotion(gimon, x, y);
    }
         
  }
  else if(timezone ==3){
   if(evening==1){
     text("今日もお疲れ！",x,y);
     emotion(smile, x, y);
   } 
   if(evening==2){
     text("こっからバイトか\nきついな",x,y);
     emotion(cry, x, y);
   } 
   if(evening==3){
     text("夕飯は肉だ！！",x,y);
     emotion(smile, x, y);
   } 
   if(evening==4){
     text("明日のテスト勉強…",x,y);
     emotion(cry, x, y);
   } 
   if(evening==5){
     text("明日\n語学当てられるよ",x,y);
     emotion(jito, x, y);
   } 
   if(evening==6){
     text("ギターが\n泣いてるぞ",x,y);
     emotion(angry, x, y);
   } 
   if(evening==7){
     text("歯磨け",x,y);
     emotion(angry, x, y);
   } 
   if(evening==8){
     text("寝てばかりだな",x,y);
     emotion(jito, x, y);
   } 
   if(evening==9){
     text("時間割を\n合わせておこう",x,y);
     emotion(smile, x, y);
   } 
   if(evening==10){
     text("アニメ\n見っっっっぞ！！",x,y);
     emotion(smile, x, y);
   } 
  }

}

void emotion(PImage p,float x,float  y){
  println("emotion");
  if(Rflag){
    println("emotion.x:"+x+"emotion.y:"+y);
    image(p,x+15,y-90);
  }
  if (Lflag) {
    image(p,x+15,y-90);
    println("emotion.x:"+x+"emotion.y:"+y);
  }
  if (Uflag) {
    image(p,x+15,y+30);    
    println("emotion.x:"+x+"emotion.y:"+y); 
  }
}


void zikanwari(){
  int tashi=80;
  int tasuX=10;
  if(cod == 1){
    for (int i = 0 ; i < mon.length; i++) {
      fill(255);
      rect(8+tasuX,58+20*i+tashi,150,14);
      if(mon[i].indexOf('空')!= 0){
        
        fill(0);       
        text((i+1)+" "+mon[i],10+tasuX,70+20*i+tashi);
      }
    }
    
  }
  else if(cod == 2){
    for (int i = 0 ; i < tue.length; i++) {
      fill(255);
      rect(8+tasuX,58+20*i+tashi,130,14);
      if(tue[i].indexOf('空')!= 0){
        
        fill(0);       
        text((i+1)+" "+tue[i],10+tasuX,70+20*i+tashi);
      }
    }
    
  }
  else if(cod == 3){
    for (int i = 0 ; i < tue.length; i++) {
      fill(255);
      rect(8+tasuX,58+20*i+tashi,150,14);
      if(wed[i].indexOf('空')!= 0){
        
        fill(0);       
        text((i+1)+" "+wed[i],10+tasuX,70+20*i+tashi);
      }
    }
  }
  else if(cod == 4){
    for (int i = 0 ; i < tue.length; i++) {
      fill(255);
      rect(8+tasuX,58+20*i+tashi,150,14);
      if(thu[i].indexOf('空')!= 0){
        
        fill(0);       
        text((i+1)+" "+thu[i],10+tasuX,70+20*i+tashi);
      }
    }
    
    
  }
  else if(cod == 5){
    for (int i = 0 ; i < fri.length; i++) {
      fill(255);
      rect(8+tasuX,58+20*i+tashi,150,14);
      if(fri[i].indexOf('空')!= 0){
        
        fill(0);       
        text((i+1)+" "+fri[i],10+tasuX,70+20*i+tashi);
      }
    }
    
  }

}
void weather(){
  weather.update();
  int code = weather.getWeatherConditionCode();
  println("tenki"+code);
  if(code == 0 || code == 1 || code == 2 || code == 3|| code ==23|| code ==37|| code ==38|| code ==39){
    println("台風系");
  }
  if(code == 5|| code == 6|| code ==10){
  println("みぞれ");
  }
  if(code ==20|| code == 21|| code ==22){
   println("霧系"); 
    
  }
  if(code == 11 || code ==12|| code ==40|| code ==45){
   image(kumori,495,30);
  }
  if(code == 13|| code ==14|| code ==15|| code ==16|| code ==17|| code ==18|| code ==19|| code ==41|| code ==42|| code ==43){
   image(yuki,495,30);
  }
  if(code ==44||code ==22 || code ==23|| code ==26|| code ==27|| code ==28|| code ==29|| code ==30){
    image(kumori,495,30);
  }
  if(code == 24){
  println("風系");
  }
  if(code == 25){
    println("寒い系");
  }
  if(code == 30|| code ==31|| code ==32|| code ==33|| code ==34|| code ==35){
    println("晴れ");
  }
  if(code ==36){
   println("暑い系"); 
  }


}
void tokei(){
  fill(0);
  text(year()+"/"+month()+"/"+day(),10,30);
  if(minute()  < 10){
    text(hour()+":0"+str(minute()-2)+":"+second(),10,50);
  }
  else {
    text(hour()+":"+minute()+":"+second(),10,50);
  }
}
/*
text("眠いェ・・・。");
text("バス間に合うんかコレ！");
text("今日は小テスト！");
text("１限から逃げるなよ！");
text("２限は語学～");
text("電車の時間、大丈夫か？");
text("昼休みに予定なかったかな・・。");
text("プレゼミ忘れんな！");
text("アイマス最高！！");
text("３限はバス混むってばよ");
text("今日テストじゃねーか！");
text("宿題やれよ！電車で！");
text("提出物は今日の１７時まで");
text("忘れ物ないか？");
text("誰かに何か貸すんじゃないの？");
text("朝勃ちしてるね（はーと");
text("お兄ちゃん起きて～");
text("もう昼過ぎジャン・・・。");
text("ふぁぁあ・・・。");
text("「アレ」の置き場を確認しろ！");
text("ホールの授業だ充電器！");
text("鼻毛出てね？");
text("ひげ酷くね？");
text("");
text("");


*/
